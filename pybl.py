#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# cython: language_level=3, embed=True, embedsignature=True

# Copyright (C) 2019, Raphaël Droz <raphael.droz+floss@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ToDo: 1. pyinstaller 2. compilation, 3. CI, 4. setcap, 5. doc, 6. IPv6

import os, sys, re, requests, argparse, logging, socket
from netaddr import IPAddress, IPNetwork, IPSet
from requests.adapters import HTTPAdapter
from urllib.request import url2pathname

iptables_ipset_rule_number = 0
bl_name = "blacklist"
hashsize = 16384
maxelem = 65536
MAX_IP_DUMP = 1000
search_regexp = re.compile(r'\b((?:\d{1,3}\.){3}\d{1,3})(?:/(\d{1,2}))?\b')

requests_session = ipset = None

nftable_list_script = """
list ruleset
"""

nftable_append_script = """

"""

nftable_replace_script = """
# https://unix.stackexchange.com/a/538314/160185
flush ruleset ip

# table {ip_type} {bl_name}
# flush table {ip_type} {bl_name}

table {ip_type} {bl_name} {{
	set {ipv_type} {{
		type {ipv_type}_addr
		flags interval, constant
		elements = {{
                     {ipv4_elements}
                }}
        }}
	chain input {{
		type filter hook input priority -400; policy accept;
		{ip_type} saddr @{ipv_type} log prefix "Blocklist Dropped: " drop
	}}
}}
"""

class LocalFileAdapter(HTTPAdapter):

    @staticmethod
    def _chkpath(method, path):
        """Return an HTTP status for the given filesystem path."""
        if method.lower() in ('put', 'delete'):
            return 501, "Not Implemented"  # TODO
        elif method.lower() not in ('get', 'head'):
            return 405, "Method Not Allowed"
        elif os.path.isdir(path):
            return 400, "Path Not A File"
        elif not os.path.isfile(path):
            return 404, "File Not Found"
        elif not os.access(path, os.R_OK):
            return 403, "Access Denied"
        else:
            return 200, "OK"

    def close(self):
        pass

    def send(self, req, **kwargs):
        path = os.path.normcase(os.path.normpath(url2pathname(req.path_url)))
        response = requests.Response()
        response.status_code, response.reason = self._chkpath(req.method, path)
        if response.status_code == 200 and req.method.lower() != 'head':
            try:
                response.raw = open(path, 'rb')
            except (OSError, IOError) as err:
                response.status_code = 500
                response.reason = str(err)

        if isinstance(req.url, bytes):
            response.url = req.url.decode('utf-8')
        else:
            response.url = req.url

        response.request = req
        response.connection = self
        return response

def fileify(string):
    if string.startswith('@'):
        return 'file://' + string[1:]
    elif string.startswith('/'):
        return 'file://' + string
    elif string.startswith('./') or os.path.isfile(string):
        return 'file://' + os.getcwd() + '/' + string
    else:
        return string

def create_iptables_chain(input_chain, bl_name, position):
    from iptc import Rule
    found = False
    for rule in input_chain.rules:
        for match in rule.matches:
            if match.name == 'comment':
                continue
            if match.name == 'set' and match.match_set == bl_name + ' src':
                found = True

    if not found:
        logger.info("Adding iptable rule at position %d" % position)
        rule = Rule()
        rule.create_target("DROP")
        m = rule.create_match("set")
        m.match_set = ['blacklist', 'src']
        input_chain.insert_rule(rule, position = position)

def create_ipset(ipset, name, force, stype, family):
    from pyroute2.netlink.exceptions import NetlinkError
    if os.geteuid() != 0 and not os.getenv("SUDO_USER"):
        logger.warning("Does not seem to be root (needed for ipset management")
    set_exist = False
    try:
        ipset.headers(name)
        set_exist = True
    except NetlinkError as e:
        if not force:
            print("Create ipset manually using: ipset create {0} -exist {1} family {2} hashsize {3} maxelem {4}".format(name, stype, family, hashsize, maxelem))
            sys.exit(1)
    if not set_exist:
        prod_ipset = ipset.create(name,
                                  stype=stype,
                                  exclusive=False,
                                  family=socket.AF_INET if family == 'inet' else socket.AF_INET6,
                                  hashsize=hashsize,
                                  maxelem=maxelem)

def get_lines(url, safe_stdin):
    if url == '-' or re.match("^&[3-9]$", url):
        if not safe_stdin:
            logger.info("Unsafe fetch, ignoring line {0}".format(url))
            return None
        if url == '-':
            return sys.stdin.readlines()
        else:
            with os.fdopen(int(url[1]), 'r') as open_fd:
                return open_fd.readlines()
    else:
        try:
            r = requests_session.get(url, timeout=10)
            if r.status_code != 200:
                logger.warning("Fetching remote blacklist {0} failed with {1}".format(url, r.status_code))
                return None
        except requests.exceptions.ReadTimeout as e:
            print(e)
            return None
        return r.content.splitlines(False)

def line_to_elements(line, remove_local):
    try:
        s = line.decode('ascii').strip()
    except AttributeError:
        s = line.strip()
    if s.startswith('#'):
        yield None
    for m in search_regexp.findall(s):
        logger.debug(m)
        ip = IPNetwork('/'.join(m)) if m[1] != '' else IPAddress(m[0])
        if ip and (not remove_local or (ip.is_unicast() and not ip.is_private() and not ip.is_loopback())):
            yield ip if ip else None

def load_remote_lists(remote_lists, safe_stdin = True, remove_local = True):
    tuples = IPSet()
    found = 0
    for url in sorted(set(remote_lists)):
        lines = get_lines(url, safe_stdin)
        if not lines:
            continue
        found += 1
        logger.debug("Fetched list {0}".format(url))
        for line in lines:
            [tuples.add(ip) for ip in line_to_elements(line, remove_local) if ip]
    return (found, tuples)

def load_ips(ips):
    tuples = IPSet()
    for ip in sorted(set(ips)):
        for m in search_regexp.findall(ip):
            ip = IPNetwork('/'.join(m)) if m[1] != '' else IPAddress(m[0])
            if ip and ip.is_unicast() and not ip.is_private() and not ip.is_loopback():
                tuples.add(ip)
    return tuples

def whitelist(whitelist):
    remote_white_lists = [fileify(n).strip() for n in whitelist.split(',') if not n.startswith('#')]
    (nb_whitelist, white_tuples) = load_remote_lists(remote_white_lists, safe_stdin = True, remove_local = False)
    logger.debug("Fetched %d ip from %d whitelists" % (len(white_tuples), nb_whitelist))
    return white_tuples

"""
Implementation based on python-nftables
- Needs and python3-nftables and libnftables1 >= 0.9.1-2
- Nuitka's git master
"""
def setup_nftable(tuples, force, cmd, level):
    logger.debug("Setting up ipset using nftables, debug = %d" % (0x79 if logger.isEnabledFor(level) else 0x00))
    from nftables import Nftables
    # Add implicitImport for libnftables when using nftables, see https://github.com/Nuitka/Nuitka/commit/fd824fd7a671186d4726104c8995aa6e25931751
    # .... or use an explicit/hinted import mechanism if one exist (bug-report for this)
    # See: https://git.netfilter.org/nftables/tree/py/nftables.py#n67
    nftables = Nftables('libnftables.so.1')
    nftables.set_debug(0x79 if logger.isEnabledFor(level) else 0x00)

    elements = []
    for ranges in tuples.iter_ipranges():
        for cidr in ranges.cidrs():
            elements.append(str(cidr))

    logger.debug(elements)
    (rc, output, error) = nftables.cmd(cmd.format(bl_name = bl_name,
                                                  ipv4_elements = ','.join(elements),
                                                  ip_type = 'ip',
                                                  ipv_type = 'ipv4')) # Resp. (ip6 / ipv6)
    # (rc, output, error) = nftables.cmd(nftable_list_script)
    print(rc, output, error)

"""
Not implemented:
A more programmatic-friendly implemention.
Once pyroute2 nftable driver is up to the task in term of nftables support.
"""
def setup_pyroute2(tuples, force, append):
    from pyroute2 import NFTables as nft
    logger.debug("Setting up ipset using pyroute2-nftables")
    # Implementation is missing:
    # - nft.set
    # - chain: priority
    # - rule: saddr(ipv4addr(@named-set))
    from pyroute2.nftables.expressions import ipv4addr, verdict

    nft.table('add', type='ip', name=bl_name)
    nft.chain('add',
              table=bl_name,
              name='input_A',
              type='filter',
              hook='input', # (priority=100)
              priority=100,
              policy=1) # accept
    nft.rule('add',
             table=bl_name,
             chain='input_A',
             expressions=(ipv4addr(src='@foobar'), genex('LOG', {'PREFIX': 'Blocklist Dropped:'}), verdict(code=0)))

"""
Stable implementation based on pyroute2_ipset
"""
def setup_pyroute2_ipset(tuples, force, append):
    from pyroute2.ipset import IPSet as pyroute2_ipset, _IPSetError
    logger.debug("Setting up ipset using pyroute2-ipset")
    stype = "hash:net"
    ipset = pyroute2_ipset()
    create_ipset(ipset, bl_name, force, stype, 'inet')
    if not append:
        bl_tmp_name = bl_name + '-tmp'
        try:
            ipset.destroy(bl_tmp_name)
        except _IPSetError as e:
            if e.code == 2:
                pass
            else:
                raise(e)
        create_ipset(ipset, bl_tmp_name, True, stype, 'inet')

    for ranges in tuples.iter_ipranges():
        for cidr in ranges.cidrs():
            logger.debug(cidr)
            try:
                ipset.add(bl_name if append else bl_tmp_name, str(cidr), etype=stype.split(':')[1])
            except _IPSetError as e:
                pass # print(e.code)

    if not append:
        ipset.swap(bl_tmp_name, bl_name)
        ipset.destroy(bl_tmp_name)

    l = None
    for i in ipset.list(bl_name)[0]['attrs']:
        if i[0] == 'IPSET_ATTR_DATA':
            for j in i[1]['attrs']:
                if j[0] == 'IPSET_ATTR_ELEMENTS':
                    l = j[1]
                    logger.info("IPSet size: %d" % l)
                    break

def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="""
Load IP address to blacklist from a variety of sources.
Blacklisting IP can be done in 3 different way:
  1. Specifying individual IPs.
  2. Specifying individual blacklists, each listing one IP per line.
  3. Specifying one source of sources listing one source per line.
""")
    parser.add_argument('-v', '--verbose', action='count', default=0, help='More verbose')
    parser.add_argument('-w', '--whitelist', help='Specify a whitelist')
    parser.add_argument('-f', '--force', action='store_true', help='Force ipset creation if it does not already exists.', default=False)
    parser.add_argument('-O', '--stdout', action='store_true', help='Output the list of IP instead of loading them in an IP set [unpriviledged operation].', default=False)
    parser.add_argument('--experimental', action='store_true', help='Use python-nftables.', default=False)
    parser.add_argument('--append', action='store_true', help='Append to already existing ipset')

    subparsers = parser.add_subparsers(title='modes', description='Valid source modes')
    parser_ip = subparsers.add_parser('ip', help='Individual IP mode')
    parser_ip.add_argument('ip', nargs='+', metavar='ips', help='Blacklist individual IP address or ranges')
    parser_list = subparsers.add_parser('blacklists', help='Load blacklists specified')
    parser_list.add_argument('lists', nargs='+', metavar='blacklists', help='Blacklists to use')
    parser_listoflists = subparsers.add_parser('metalist', help='List of lists')
    parser_listoflists.add_argument('metalist', metavar='metalist', nargs=1, help='Point to a list of blacklists to use (one per line)')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    level = levels[min(len(levels)-1, args.verbose)]
    logger = logging.getLogger()
    logger.setLevel(level)
    handler = logging.StreamHandler()
    handler.setLevel(level)
    logger.addHandler(handler)

    requests_session = requests.Session()
    requests_session.mount('file://', LocalFileAdapter())

    remote_lists = []
    safe_stdin = True
    if 'lists' in args:
        remote_lists = [fileify(n) for n in args.lists]
    elif 'metalist' in args:
        for l in [fileify(n) for n in args.metalist]:
            if l == '-':
                remote_lists.extend(sys.stdin.readlines())
            else:
                r = requests_session.get(l, timeout=3)
                if r.status_code != 200:
                    logger.warning("Fetching meta list {0} failed with {1}".format(args.metalist[0], r.status_code))
                    sys.exit(1)
                else:
                    safe_stdin = False
                    remote_lists.extend([l.decode('utf-8').strip() for l in r.content.splitlines(False)])

    tuples = []
    if 'ip' in args:
        tuples = load_ips(args.ip)
        logger.debug("Fetched %d ip from command-line" % len(tuples))
    elif len(remote_lists) > 0:
        remote_lists = [fileify(n).strip() for n in remote_lists if not n.startswith('#')]
        (nb_lists, tuples) = load_remote_lists(remote_lists, safe_stdin)
        logger.debug("Fetched %d ip from %d remote blacklists" % (len(tuples), nb_lists))

    if len(tuples) < 1:
        logger.info("No IPs")
        sys.exit(0)

    if args.whitelist:
        prev_len = len(tuples)
        tuples = tuples - whitelist(args.whitelist)
        logger.debug("Blacklist down from %d to %d" % (prev_len, len(tuples)))

    if args.stdout:
        if level < logging.WARNING: # at least "verbose"
            if len(tuples) < MAX_IP_DUMP or args.force:
                logger.info([str(t) for t in tuples])
        print("ipset size: %d" % len(tuples))
        sys.exit(0)

    if args.experimental:
        setup_nftable(tuples, args.force, nftable_append_script if args.append else nftable_replace_script, level)
    else:
        from iptc import Chain, Table
        setup_pyroute2_ipset(tuples, args.force, args.append)
        input_chain = Chain(Table(Table.FILTER), "INPUT")
        create_iptables_chain(input_chain, bl_name, iptables_ipset_rule_number)
