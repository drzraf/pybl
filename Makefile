PYTHON ?= python
CYTHON ?= cython
TOP := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

PYVERSION := $(shell $(PYTHON) -c "import sys; print(sys.version[:3])")
PYPREFIX := $(shell $(PYTHON) -c "import sys; print(sys.prefix)")

INCDIR := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_python_inc())")
PLATINCDIR := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_python_inc(plat_specific=True))")
LIBDIR1 := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_config_var('LIBDIR'))")
LIBDIR2 := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_config_var('LIBPL'))")
PYLIB := $(shell $(PYTHON) -c "from distutils import sysconfig; print(sysconfig.get_config_var('LIBRARY')[3:-2])")

CC := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('CC'))")
LINKCC := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('LINKCC'))")
LINKFORSHARED := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('LINKFORSHARED'))")
LIBS := $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('LIBS'))")
SYSLIBS :=  $(shell $(PYTHON) -c "import distutils.sysconfig; print(distutils.sysconfig.get_config_var('SYSLIBS'))")

requirements:
	pip install -r requirements.txt

pybl: pybl.o
	$(LINKCC) -o build/$@-$(PYVERSION) $^ -L$(LIBDIR1) -L$(LIBDIR2) -l$(PYLIB) $(LIBS) $(SYSLIBS) $(LINKFORSHARED)

pybl.o: pybl.c
	$(CC) -c $^ -I$(INCDIR) -I$(PLATINCDIR)

pybl.c: pybl.py
	$(CYTHON) --embed pybl.py

pybl-nut: pybl.py
	# $(PYTHON) -m nuitka -o build/$@-$(PYVERSION) --include-module=iptc --include-module=libxtwrapper --include-module=netaddr --include-module=pyroute2 --experimental=use_pefile_recurse --experimental=use_pefile_fullrecurse -j 4 --show-scons --show-progress --show-memory --show-modules --verbose pybl.py
	$(PYTHON) -m nuitka -o build/$@-$(PYVERSION) --include-module=netaddr --experimental=use_pefile_recurse --experimental=use_pefile_fullrecurse -j 4 --show-scons --show-progress --show-memory --show-modules --verbose pybl.py

# patchelf --replace-needed libpython3.7m.so.1.0 libpython3.5m.so.1 embedded
# nuitka3 -o pybl pybl.py
# nuitka3 --unstripped --standalone --experimental=use_pefile_recurse --experimental=use_pefile_fullrecurse -j 4 --show-scons  --show-progress --show-memory --show-modules --verbose pybl.py
# nuitka3 -o build/pybl-hinted-3.7 --recurse-none --experimental=use_pefile --user-plugin=hinted-mods.py=ci/pybl-3.7.3-linux-64.json -j2 --show-scons --show-progress --show-memory --show-modules --verbose --recompile-c-only pybl.py
docker: requirements pybl-nut
	:

pybl-3.7:
	docker run --volume "$(TOP):/tmp/project" -ti python:$(subst pybl-,,$@) sh -c "make -BC /tmp/project docker"
pybl-3.6:
	docker run --volume "$(TOP):/tmp/project" -ti python:$(subst pybl-,,$@) sh -c "make -BC /tmp/project docker"
pybl-3.5:
	docker run --volume "$(TOP):/tmp/project" -ti python:$(subst pybl-,,$@) sh -c "make -BC /tmp/project docker"
