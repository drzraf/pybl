from pyroute2.netlink.exceptions import NetlinkError
from pyroute2.ipset import IPSet as pyroute2_ipset, _IPSetError
from socket import AF_INET
if __name__ == '__main__':
    ipset = pyroute2_ipset()
    ipset.destroy("test")
    ipset.create("test", stype="hash:net", family=AF_INET)
    ipset.add("test", "99.99.99.99/32", etype="net")
