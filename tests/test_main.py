from subprocess import run, PIPE
import tempfile, re

exe = './pybl.py'
re_type = type(re.compile('foo'))

def _run_test(cmd, expected_output = None, expected_error = None):
    cmd = cmd.format(exe=exe)
    r = run(cmd, shell=True, executable='/bin/bash', capture_output=True)
    if expected_output:
        if isinstance(expected_output, re_type):
            assert expected_output.match(r.stdout.decode('UTF-8'))
        else:
            assert r.stdout.decode('UTF-8').strip() == expected_output.decode('UTF-8').strip()
    if expected_error:
        assert r.stderr.decode('UTF-8').strip() == expected_error.decode('UTF-8').strip()
    if not expected_output and not expected_error:
        return r.stdout.decode('UTF-8').strip(), r.stderr.decode('UTF-8').strip()

def _mktfile(string):
    tf = tempfile.NamedTemporaryFile()
    tf.write(string)
    tf.seek(0)
    return tf

def test_fd():
    cmd = 'exec 3<<<"99.99.99.99/32" && echo "&3" | {exe} -vO metalist -'
    err = b"['99.99.99.99']"
    out = b"ipset size: 1"
    _run_test(cmd, out, err)

def test_empty_set():
    cmd = 'echo 8.8.8.0/24 | {exe} -vO -w -'
    out = b""
    err = b"No IPs"
    _run_test(cmd, out, err)

def test_whitelist():
    cmd = 'echo 8.8.8.0/24 | {exe} -vO -w - ip 1.1.1.1'
    out = b"ipset size: 1"
    err = b"['1.1.1.1']"
    _run_test(cmd, out, err)

def test_whitelist_stdin():
    cmd = """
exec 4<<<$'1.1.1.0/25\n1.1.1.128/26\n1.1.1.192/27' && exec 3<<<"1.1.1.0/24" && echo "&3" | {exe} -Ow "&4" metalist -
"""
    out = b"ipset size: 32"
    _run_test(cmd, out)

def test_remote_metalist():
    meta = _mktfile(b"""\
https://www.spamhaus.org/drop/drop.lasso
""")
    cmd = "{exe} -O metalist " + meta.name
    out = re.compile(r"^ipset size: \d{3,}$")
    _run_test(cmd, out)

def test_comment_metalist():
    meta = _mktfile(b"# this is a comment")
    cmd = "{exe} -O metalist " + meta.name
    out = b""
    _run_test(cmd, out)

def test_localfile_metalist():
    alist = _mktfile(b"1.1.1.2\n1.1.1.1")
    meta = _mktfile(alist.name.encode())
    cmd = "{exe} -O metalist " + meta.name
    out = b"ipset size: 2"
    _run_test(cmd, out)
