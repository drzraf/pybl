#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, Extension, find_packages
# from distutils.core import setup
from distutils.cmd import Command
from Cython.Build import cythonize
              
setup(
    name='pybl',
    version='0.0.1',
    url='https://gitlab.com/drzraf/pybl',
    license='GPL-3+',
    author='Raphaël Droz',
    author_email='raphael@droz.eu',
    description='Python implementation of ipset-blacklist',
    tests_require=["pytest"],
    setup_requires=['pytest-runner'],
    requires=['iptc', 'netaddr', 'pyroute2', 'requests', 'urllib', 'Cython'],
    command_options={ 'nuitka3' : { '--unstripped': True,
                                   '--standalone': True,
                                   '--experimental': 'use_pefile_recurse',
                                   '--experimental': 'use_pefile_fullrecurse',
                                   '--jobs': 4,
                                   '--show-scons': True,
                                   '--show-progress': True,
                                   '--show-memory': True,
                                   '--show-modules': True,
                                   '--verbose': True,
                                   '--file-reference-choice': 'original'},
                      'nuitka-int' : {
                          '--output': 'pybl'
                      }
    },
    entry_points={
        'console_scripts': [
            'pybl = pybl.py:main'
        ]
    },
    zip_safe=False,
    include_package_data=True,
    packages=find_packages(),
    # ext_modules=cythonize([Extension(name='pybl', sources=['pybl.py'])])
)

# To compile Cython: ./setup.py build_ext --inplace
# Warn: when compiled Python `subprocess` *needs* /sbin/ldconfig to be available from PATH
